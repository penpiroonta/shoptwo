package main

import (
	"shopetwo/controllers"
	"shopetwo/repositories"

	"github.com/labstack/echo/v4"
)

func main() {

	repositories.InnittialDB()
	db, _ := repositories.ConnectionPool.DB()
	defer db.Close()

	e := echo.New()

	e.POST("/createshoptwo", controllers.CreateShoptwoColltroler)

	e.PUT("/shopname/:id", controllers.UpdateShoptwoColltroler)

	e.DELETE("/shopname/:id", controllers.DeleteShoptwoColltroler)

	e.GET("/shoptwo", controllers.GetShoptwoControllers)

	e.GET("/shopname/:id", controllers.GetShoptwoSelectControllers)

	e.Logger.Fatal(e.Start(":1323"))

}
