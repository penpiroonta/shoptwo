package models

type Shoptwo struct {
	ID                 int    `json:"id"`
	ShopName           string `json:"shop_name"`
	ProductName        string `json:"product_name"`
	UnitPrince         int    `json:"unit_prince"`
	UnitMeasure        string `json:"unit_measure"`
	ProductCategory    string `json:"product_category"`
	ProductImage       string `json:"product_image"`
	ProductDescription string `json:"product_description"`
}

func (s Shoptwo) TableName() string {
	return "shoptwo"

}
