As a shop manager, I can view create, 
update the view and delete my shop product information 
( shop, product name, unit price, unit 
of measure, product 
category, product image, product description) 
using API ( JSON Restful Web Service)  

create  POST  /createshoptwo
update  PUT   /shopname/:id
delete  delete /shopname/:id
