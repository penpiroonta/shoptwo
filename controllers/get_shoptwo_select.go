package controllers

import (
	"shopetwo/repositories"
	"strconv"

	"github.com/labstack/echo/v4"
)

func GetShoptwoSelectControllers(ctx echo.Context) error {

	idString := ctx.Param("id")

	id, _ := strconv.Atoi(idString)

	shops := repositories.GetShopSelecttwoRepo(id)
	return ctx.JSON(200, map[string]interface{}{
		"data": shops,
	})

}
