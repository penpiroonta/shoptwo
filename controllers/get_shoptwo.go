package controllers

import (
	"shopetwo/repositories"

	"github.com/labstack/echo/v4"
)

func GetShoptwoControllers(ctx echo.Context) error {

	shoptwo := repositories.GetShoptwoRepo()
	return ctx.JSON(200, map[string]interface{}{
		"data": shoptwo,
	})

}
