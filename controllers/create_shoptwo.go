package controllers

import (
	"shopetwo/models"
	"shopetwo/repositories"

	"github.com/labstack/echo/v4"
)

func CreateShoptwoColltroler(ctx echo.Context) error {

	shopNameRequest := new(models.Shoptwo)

	ctx.Bind(&shopNameRequest)

	if shopNameRequest.ShopName == "" {
		return ctx.JSON(400, map[string]interface{}{
			"error": "ไม่ถูกต้อง",
		})
	}

	shoptwo := repositories.CreateShoptwoRepo(*shopNameRequest)

	return ctx.JSON(200, map[string]interface{}{
		"data": shoptwo,
	})
}
