package controllers

import (
	"shopetwo/models"
	"shopetwo/repositories"
	"strconv"

	"github.com/labstack/echo/v4"
)

func UpdateShoptwoColltroler(ctx echo.Context) error {

	idString := ctx.Param("id")

	id, _ := strconv.Atoi(idString)

	shops := new(models.Shoptwo)

	ctx.Bind(&shops)

	shops.ID = id

	err := repositories.UpdateShoptwoRepo(*shops)

	if err != nil {
		return ctx.JSON(400, map[string]interface{}{
			"error": err.Error(),
		})
	}

	return ctx.JSON(200, map[string]interface{}{
		"data": "susscess",
	})

}
