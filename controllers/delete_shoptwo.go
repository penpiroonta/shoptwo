package controllers

import (
	"shopetwo/repositories"
	"strconv"

	"github.com/labstack/echo/v4"
)

func DeleteShoptwoColltroler(ctx echo.Context) error {

	idString := ctx.Param("id")

	id, _ := strconv.Atoi(idString)
	err := repositories.DeleteShoptwoRepo(id)

	if err != nil {
		ctx.JSON(400, map[string]interface{}{
			"error": err.Error(),
		})
	}
	return ctx.JSON(200, map[string]interface{}{
		"data": "susscess",
	})

}
