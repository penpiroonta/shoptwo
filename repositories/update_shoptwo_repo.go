package repositories

import (
	"shopetwo/models"
)

func UpdateShoptwoRepo(shops models.Shoptwo) error {

	return ConnectionPool.Debug().Where("id=?", shops.ID).Updates(&shops).Error

}
