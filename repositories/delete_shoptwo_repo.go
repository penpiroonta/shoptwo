package repositories

import (
	"shopetwo/models"
)

func DeleteShoptwoRepo(id int) error {

	return ConnectionPool.Where("id=?", id).Delete(models.Shoptwo{}).Error

}
