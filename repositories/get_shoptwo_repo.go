package repositories

import "shopetwo/models"

func GetShoptwoRepo() []models.Shoptwo {

	shops := new([]models.Shoptwo)

	ConnectionPool.Find(&shops)
	return *shops

}
